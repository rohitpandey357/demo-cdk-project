import pytest  
import aws_cdk as cdk 
from aws_cdk.assertions import Template  
from demo_cdk_project.s3_bucket_stack import S3BucketStack  
  
@pytest.fixture
def s3_bucket_template():
    app = cdk.App()  
    s3_bucket_stack = S3BucketStack(app, "MyTestStack")  
    s3_bucket_template = Template.from_stack(s3_bucket_stack) 
    return s3_bucket_template

def test_s3_bucket_stack_reosurce_count_is_one(s3_bucket_template):    
    # assert the stack contains an S3 bucket  
    s3_bucket_template.resource_count_is("AWS::S3::Bucket", 1)  
  
