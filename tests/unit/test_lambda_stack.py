import pytest  
import aws_cdk as cdk 
from aws_cdk import Stack, aws_s3 as s3 
from constructs import Construct
from aws_cdk.assertions import Template
from demo_cdk_project.lambda_stack import LambdaStack 

@pytest.fixture
def lambda_template():
    app = cdk.App()  

    class S3BucketStack(Stack):
        def __init__(self, scope: Construct, construct_id: str, **kwargs) -> None:
            super().__init__(scope, construct_id, **kwargs)
            self.bucket = s3.Bucket(self, "MyFirstBucket")
    
    test_bucket = S3BucketStack(app, "S3BucketStack")   
    lambda_stack = LambdaStack(app, "LambdaStack", bucket=test_bucket.bucket)  
    lambda_template = Template.from_stack(lambda_stack)   
    return lambda_template
  
def test_lambda_stack_resource_count_is_one(lambda_template):  
    lambda_template.resource_count_is("AWS::Lambda::Function", 1)  

def test_lambda_stack_properties(lambda_template):
    lambda_template.has_resource_properties("AWS::Lambda::Function", {  
        "Handler": "index.lambda_handler",  
        "Runtime": "python3.12"  
    })   
