from aws_cdk import (
    aws_lambda as _lambda, 
    aws_iam as iam, 
    aws_s3 as s3,  
    Stack,  
)  
from constructs import Construct
  
class LambdaStack(Stack):  
    def __init__(self, scope: Construct, id: str, bucket: s3.IBucket, **kwargs):  
        super().__init__(scope, id, **kwargs)  
  
        lambda_function = _lambda.Function(  
            self, 'MyLambda',  
            code=_lambda.Code.from_inline('''  
                import boto3  
                import os
                def lambda_handler(event, context):  
                    s3 = boto3.client('s3')  
                    response = s3.list_objects(Bucket=os.environ['BUCKET_NAME'])  
                    for content in response['Contents']:  
                        print(content['Key'])  
            '''),  
            handler='index.lambda_handler',  
            runtime=_lambda.Runtime.PYTHON_3_12,  
            environment={  
                'BUCKET_NAME': bucket.bucket_name,  
            },  
            initial_policy=[  
                iam.PolicyStatement(  
                    actions=['s3:ListBucket'],  
                    resources=[bucket.bucket_arn]  
                )  
            ]  
        )  
   
        