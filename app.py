#!/usr/bin/env python3

import aws_cdk as cdk

from demo_cdk_project.s3_bucket_stack import S3BucketStack
from demo_cdk_project.lambda_stack import LambdaStack

app = cdk.App()
bucket_stack = S3BucketStack(app, "S3BucketStack")  
LambdaStack(app, "LambdaStack", bucket=bucket_stack.bucket)  

app.synth()
